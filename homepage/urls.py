from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.story3, name='story3'),
    path('story32/', views.story32, name='story32'),
    path('story33/', views.story33, name='story33'),
    path('story34/', views.story34, name='story34'),

    # dilanjutkan ...
]