from django import  forms
from .models import Kegiatan

FAVORITE_COLORS_CHOICES = [
    ('blue', 'Blue'),
    ('green', 'Green'),
    ('black', 'Black'),
]

class FormJadwal(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = ['hari', 'tanggal', 'jam', 'namaKegiatan', 'kategori']
        widgets = {
            'tanggal': forms.DateInput(attrs={'type': 'date'}),
            'jam': forms.TimeInput(attrs={'type': 'time'})}
            
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })




# class Message_Form(forms.Form):
#     hari = models.CharField(max_length=10)
#     tanggal = models.DateField()
#     jam = models.TimeField()
#     namaKegiatan = models.CharField(max_length=30)
#     tempat = models.CharField(max_length=30)
#     kategori = models.CharField(max_length=30)