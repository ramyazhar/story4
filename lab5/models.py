

# Create your models here.
from django.db import models
from django.utils import timezone
from datetime import datetime, date

KEGEG = (
        ('happy', 'Menyenangkan'),
        ('normal', 'Biasa aja'),
        ('bored', 'Membosankan'),
    )

class Kegiatan(models.Model):
    hari = models.CharField(max_length=10)
    tanggal = models.DateField()
    jam = models.TimeField()
    namaKegiatan = models.CharField(max_length=30)
    tempat = models.CharField(max_length=30)
    kategori = models.CharField(max_length=1, choices=KEGEG)