from django.urls import path
from . import views


urlpatterns = [
    path('', views.schedule, name='schedule'),
    path('create/', views.schedule_create, name='schedule_create'),
    path('delete/<int:id>/', views.schedule_delete, name='schedule_delete'),
    # dilanjutkan ...
]