from django.shortcuts import render, redirect
from .models import Kegiatan
from . import forms

def schedule(request):
    schedules = Kegiatan.objects.all().order_by('tanggal')
    return render(request, 'schedule.html', {'schedules': schedules})

def schedule_create(request):
    if request.method == 'POST':
        form = forms.FormJadwal(request.POST)
        if form.is_valid():
            form.save()
            return redirect('lab5:schedule')

    else:
        form = forms.FormJadwal()
    return render(request, 'schedule_create.html', {'form': form})

def schedule_delete(request,id):
    if request.method == 'POST':
        Kegiatan.objects.filter(id=id).delete()
        return redirect('lab5:schedule')
    else :
        return httpresponse("/Get not load")